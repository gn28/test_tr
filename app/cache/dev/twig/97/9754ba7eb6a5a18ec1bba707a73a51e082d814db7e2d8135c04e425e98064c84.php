<?php

/* base.html.twig */
class __TwigTemplate_4466a4a57a5592ebcac9e6378daafe9c0fef1f8a58b8cc94f430002013906d68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_582e794f759a881eb91410ff26abae93a2c293e135bcd738aad52bcaf68436cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_582e794f759a881eb91410ff26abae93a2c293e135bcd738aad52bcaf68436cb->enter($__internal_582e794f759a881eb91410ff26abae93a2c293e135bcd738aad52bcaf68436cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_582e794f759a881eb91410ff26abae93a2c293e135bcd738aad52bcaf68436cb->leave($__internal_582e794f759a881eb91410ff26abae93a2c293e135bcd738aad52bcaf68436cb_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_cd7b5bd9969ef74a6d14831c059f0352f52dc61bdc08dbf3549e7cbb204e7cbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd7b5bd9969ef74a6d14831c059f0352f52dc61bdc08dbf3549e7cbb204e7cbe->enter($__internal_cd7b5bd9969ef74a6d14831c059f0352f52dc61bdc08dbf3549e7cbb204e7cbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_cd7b5bd9969ef74a6d14831c059f0352f52dc61bdc08dbf3549e7cbb204e7cbe->leave($__internal_cd7b5bd9969ef74a6d14831c059f0352f52dc61bdc08dbf3549e7cbb204e7cbe_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0b4e73714d02a9324ce1ddb9479a9aee9d79feee2933192f443ba1ec250c6791 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b4e73714d02a9324ce1ddb9479a9aee9d79feee2933192f443ba1ec250c6791->enter($__internal_0b4e73714d02a9324ce1ddb9479a9aee9d79feee2933192f443ba1ec250c6791_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_0b4e73714d02a9324ce1ddb9479a9aee9d79feee2933192f443ba1ec250c6791->leave($__internal_0b4e73714d02a9324ce1ddb9479a9aee9d79feee2933192f443ba1ec250c6791_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_6ff01f14d91bf7e34ec43bb68faba8559c351f8eecb6e11d7180bdc608246fe1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ff01f14d91bf7e34ec43bb68faba8559c351f8eecb6e11d7180bdc608246fe1->enter($__internal_6ff01f14d91bf7e34ec43bb68faba8559c351f8eecb6e11d7180bdc608246fe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6ff01f14d91bf7e34ec43bb68faba8559c351f8eecb6e11d7180bdc608246fe1->leave($__internal_6ff01f14d91bf7e34ec43bb68faba8559c351f8eecb6e11d7180bdc608246fe1_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b6c4c62d69053ec30d834182edcf344c039d553075c406396fdd3341d1271972 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6c4c62d69053ec30d834182edcf344c039d553075c406396fdd3341d1271972->enter($__internal_b6c4c62d69053ec30d834182edcf344c039d553075c406396fdd3341d1271972_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_b6c4c62d69053ec30d834182edcf344c039d553075c406396fdd3341d1271972->leave($__internal_b6c4c62d69053ec30d834182edcf344c039d553075c406396fdd3341d1271972_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/gn/my_project_name/test_tr/app/Resources/views/base.html.twig");
    }
}
