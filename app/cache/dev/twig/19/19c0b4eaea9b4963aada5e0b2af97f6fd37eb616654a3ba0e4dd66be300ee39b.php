<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_a17b172c1df3a594601d3716a65d39fc4921802efc1c202912b1a7cbd69067af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e28bb1dbe2998750a0edb1e0c7e73328e95e3c7cf5a1b2c164433ec0d7d5e41f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e28bb1dbe2998750a0edb1e0c7e73328e95e3c7cf5a1b2c164433ec0d7d5e41f->enter($__internal_e28bb1dbe2998750a0edb1e0c7e73328e95e3c7cf5a1b2c164433ec0d7d5e41f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e28bb1dbe2998750a0edb1e0c7e73328e95e3c7cf5a1b2c164433ec0d7d5e41f->leave($__internal_e28bb1dbe2998750a0edb1e0c7e73328e95e3c7cf5a1b2c164433ec0d7d5e41f_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_e2b3926257a573214c78360029ff1dae9ec9cc2c750a9608a5f444c100e5882c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2b3926257a573214c78360029ff1dae9ec9cc2c750a9608a5f444c100e5882c->enter($__internal_e2b3926257a573214c78360029ff1dae9ec9cc2c750a9608a5f444c100e5882c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_e2b3926257a573214c78360029ff1dae9ec9cc2c750a9608a5f444c100e5882c->leave($__internal_e2b3926257a573214c78360029ff1dae9ec9cc2c750a9608a5f444c100e5882c_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e29671846c4e2238ff99670b426d0648d37d6675f4bf1f69c755a38d81f79331 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e29671846c4e2238ff99670b426d0648d37d6675f4bf1f69c755a38d81f79331->enter($__internal_e29671846c4e2238ff99670b426d0648d37d6675f4bf1f69c755a38d81f79331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e29671846c4e2238ff99670b426d0648d37d6675f4bf1f69c755a38d81f79331->leave($__internal_e29671846c4e2238ff99670b426d0648d37d6675f4bf1f69c755a38d81f79331_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_773c018a3cfbc4808811f50d9a4a95f7bb8127b8aae7121bcfbdc1c226f2f48f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_773c018a3cfbc4808811f50d9a4a95f7bb8127b8aae7121bcfbdc1c226f2f48f->enter($__internal_773c018a3cfbc4808811f50d9a4a95f7bb8127b8aae7121bcfbdc1c226f2f48f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_773c018a3cfbc4808811f50d9a4a95f7bb8127b8aae7121bcfbdc1c226f2f48f->leave($__internal_773c018a3cfbc4808811f50d9a4a95f7bb8127b8aae7121bcfbdc1c226f2f48f_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/gn/my_project_name/test_tr/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
