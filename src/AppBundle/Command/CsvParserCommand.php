<?php
/**
 * Created by PhpStorm.
 * User: gn
 * Date: 6/5/17
 * Time: 12:08 PM
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use AppBundle\Entity\Tblproductdata;

class CsvParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->addArgument('mode', InputArgument::OPTIONAL, 'mode?')
            ->setName('app:csv-parser')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to parse csv file...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mode = $input->getArgument('mode');

        header('Content-Type: text/html; charset=utf-8');

        $handle = fopen($this->getContainer()->get('kernel')->getRootDir() . '/../web/product/stock.csv', "r+");

        $line = 0;
        $em = $this->getContainer()->get('doctrine')->getManager();
        $q = $em->createQuery('delete from AppBundle\Entity\Tblproductdata');
        $q->execute();

        $log = array();

        while (!feof($handle)) {
            $matches = fgetcsv($handle, 0, ',');

            if (!$line) {
                $line++;
                continue;
            }



            if (isset($matches[3]) && isset($matches[4])) {
                if ($matches[4] > 5 && $matches[4] < 1000) {
                    $product = new Tblproductdata();
                    $product->setStrproductname($matches[1]);
                    $product->setStrproductdesc($matches[2]);
                    $product->setStrproductcode($matches[0]);



                    // tells Doctrine you want to (eventually) save the Product (no queries yet)
                    $em->persist($product);
                    unset($product);

                } else {
                    $log[] = 'Error while inserting: ' . $matches[0] . ' ' . $matches[1];
                }
            } else {
                $log[] = 'Error while inserting: ' . $matches[0] . ' ' . $matches[1];
            }

        }

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        fclose($handle);

        print_r($log);

    }
}